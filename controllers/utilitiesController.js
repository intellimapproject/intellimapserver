'use strict';
console.info("file utilitiesController.js loading");
const responseMessage = require('./../responseMessage.json');
const validator = require('validator');
const HTTPStatus = require('http-status');

/*this method is used to return all response to client */
function returnResponse(res, status, isSuccessful, data) {
    console.info("Response status:" + status + " Successful:" + isSuccessful);
    if (isSuccessful) {
        res.status(status).send({
            result: responseMessage.ok,
            data: data
        });
    }
    else {
        res.status(status).send({
            result: responseMessage.fail,
            error: data
        });
    }
}

exports.returnResponse = returnResponse;

exports.isStringNullUndefinedOrEmpty = function (str) {
    return str === undefined || str === null || validator.isEmpty(str)
};

exports.getObjectsFromDb = function (res, query) {
    query.exec(function (err, doc) {
        if (err) {
            console.error(err);
            returnResponse(res, HTTPStatus.INTERNAL_SERVER_ERROR, false, responseMessage.dbError);
        }
        else if (doc.length === 0) {
            console.error("Not found");
            returnResponse(res, HTTPStatus.OK, true, []);
        }
        else {
            returnResponse(res, HTTPStatus.OK, true, doc);
        }
    });
};

exports.getObjectFromDb = function (res, query, objectId, objectType) {
    query.exec(function (err, doc) {
        if (err) {
            console.error(err);
            returnResponse(res, HTTPStatus.INTERNAL_SERVER_ERROR, false, responseMessage.dbError);
        }
        else if (doc.length === 0) {
            console.info("Object not found");
            returnResponse(res, HTTPStatus.NOT_FOUND, false, responseMessage.spaceUnitNotFound);
        }
        else if (doc.length > 1) {
            console.error("Critical error." + objectType + " object" + objectId + "not unique");
            returnResponse(res, HTTPStatus.INTERNAL_SERVER_ERROR, false, responseMessage.serverError);
        }
        else {
            returnResponse(res, HTTPStatus.OK, true, doc[0]);
        }
    });
};


exports.getUniqueObjectValidation = function (doc, objectType, id) {
    if (doc.length === 0) {
        console.error(objectType + " not found");
        throw {
            message: responseMessage.spaceUnitNotFound,
            httpStatus: HTTPStatus.NOT_FOUND
        };
    }
    else if (doc.length > 1) {
        console.error("Critical error. " + objectType + " " + id + "not unique");
        throw {
            message: responseMessage.serverError,
            httpStatus: HTTPStatus.INTERNAL_SERVER_ERROR
        };
    }
};

/*this method is used to return all error response to client */
exports.returnErrorToClient = function (res, error) {
    let httpStatus;
    let message;
    if (error) {
        if (error.httpStatus === undefined) {
            httpStatus = HTTPStatus.INTERNAL_SERVER_ERROR;
            message = responseMessage.serverError;
        }
        else {
            httpStatus = error.httpStatus;
            message = error.message;
        }
    }
    console.error(error);
    console.error(message);
    returnResponse(res, httpStatus, false, message);
};


