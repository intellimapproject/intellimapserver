'use strict';
console.info("file userController.js loading");
const HTTPStatus = require('http-status');
const bcrypt = require('bcryptjs');
const passport = require('passport');
const utilitiesController = require('./utilitiesController');
const roles = require('./../roles.json');
const responseMessage = require('../responseMessage.json');
const userSchema = require('./../db/userSchema');

exports.register = function (req, res) {
    if (!((utilitiesController.isStringNullUndefinedOrEmpty(req.query.name))
            || utilitiesController.isStringNullUndefinedOrEmpty(req.query.email)
            || utilitiesController.isStringNullUndefinedOrEmpty(req.query.username)
            || utilitiesController.isStringNullUndefinedOrEmpty(req.query.password)
        )) {
        const name = req.query.name;
        const email = req.query.email;
        const username = req.query.username;
        const password = req.query.password;
        let saltRounds = 10;
        let promise = bcrypt.hash(password, saltRounds);
        promise.then(hash => {
            const newUser = new userSchema({
                name: name,
                email: email,
                username: username,
                role: roles.user,
                password: hash
            });
            return newUser.save();
        }).then(user => {
            let result = {
                name: user.name,
                email: user.email,
                username: user.username,
                role: user.role,
                id: user._id
            };
            utilitiesController.returnResponse(res, HTTPStatus.CREATED, true, result);
        }).catch(err => {
            utilitiesController.returnErrorToClient(res, err);
        });
    } else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};

exports.successLogin = function (req, res) {
    if (req.user !== null) {
        let user = req.user;
        let result = {
            name: user.name,
            email: user.email,
            username: user.username,
            role: user.role,
            id: user._id
        };
        utilitiesController.returnResponse(res, HTTPStatus.OK, true, result);
    }
    else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};

exports.logout = function (req, res) {
    console.info("logout");
    req.logout();
    utilitiesController.returnResponse(res, HTTPStatus.OK, true, responseMessage.ok);
};

exports.failureLogin = function (req, res) {
    utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, "FAIL");
};

exports.getUserByUsername = function (username, callback) {
    let query = {username: username};
    userSchema.findOne(query, callback);
};

exports.getUserById = function (id, callback) {
    userSchema.findById(id, callback);
};

exports.comparePassword = function (candidatePassword, hash, callback) {
    bcrypt.compare(candidatePassword, hash, function (err, isMatch) {
        if (err) throw err;
        callback(null, isMatch);
    });
};

exports.getAllUsersList = function (req, res) {
    const query = userSchema.find().where({});
    let queryExePromise = query.exec();
    queryExePromise.then(doc => {
        let result = [];
        doc.forEach(user => {
            result.push({
                name: user.name,
                email: user.email,
                username: user.username,
                role: user.role,
                id: user._id
            })
        });
        utilitiesController.returnResponse(res, HTTPStatus.OK, true, result);
    }).catch(err => {
        utilitiesController.returnErrorToClient(res, err);
    });
};

exports.getUsersByIds = function (req, res) {
    if (!( utilitiesController.isStringNullUndefinedOrEmpty(req.query.usersIds))) {
        let usersIds = req.query.usersIds.split(',');
        let usersIdsArray = [];
        usersIds.forEach(id => {
            usersIdsArray.push({
                _id: id
            });
        });
        const query = userSchema.find().where({
            $or: usersIdsArray
        });
        let queryExePromise = query.exec();
        queryExePromise.then(doc => {
            let result = [];
            doc.forEach(user => {
                result.push({
                    name: user.name,
                    email: user.email,
                    username: user.username,
                    role: user.role,
                    id: user._id
                })
            });
            utilitiesController.returnResponse(res, HTTPStatus.OK, true, result);
        });
    }
    else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};