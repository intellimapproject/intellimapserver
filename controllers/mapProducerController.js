'use strict';
console.info("file mapProducerController.js loading");
const HTTPStatus = require('http-status');
const utilitiesController = require('./utilitiesController');
const navigationController = require('./navigationController');
const mapConsumerController = require('./mapConsumerController');
const responseMessage = require('../responseMessage.json');
const spaceUnitSchema = require('./../db/spaceUnitSchema');
const sensorSchema = require('./../db/sensorSchema');
const tagSchema = require('./../db/tagSchema');
const spaceUnitTypes = require('../spaceUnitTypes.json');
const eventSchema = require('./../db/eventSchema');
const config = require('../config.json');

exports.createSite = function (req, res) {
    if (!((utilitiesController.isStringNullUndefinedOrEmpty(req.query.siteName))
        || utilitiesController.isStringNullUndefinedOrEmpty(req.query.siteDescription))) {
        const siteName = req.query.siteName;
        const siteDescription = req.query.siteDescription;
        const spaceUnitSite = new spaceUnitSchema(
            {
                type: spaceUnitTypes.site,
                name: siteName,
                description: siteDescription,
                buildingId: -1,
                floorId: -1,
                parentId: -1
            });
        let querySavePromise = spaceUnitSite.save();
        querySavePromise.then(doc => {
            doc.siteId = doc._id;
            doc.spaceUnitId = doc._id + ".0.0.0";
            return doc.save();
        }).then(docUpdate => {
            utilitiesController.returnResponse(res, HTTPStatus.CREATED, true, docUpdate);
        }).catch(err => {
            utilitiesController.returnErrorToClient(res, err);
        });
    }
    else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};

exports.editSite = function (req, res) {
    const siteName = req.query.siteName;
    const siteDescription = req.query.siteDescription;
    const siteId = req.params.siteId;
    const query = spaceUnitSchema.find().where({
        $and: [{siteId: siteId}, {type: spaceUnitTypes.site}]
    });
    let queryExePromise = query.exec();
    queryExePromise.then(doc => {
        utilitiesController.getUniqueObjectValidation(doc, spaceUnitTypes.site, siteId);
        return doc;
    }).then(doc => {
        if (!utilitiesController.isStringNullUndefinedOrEmpty(siteName)) {
            doc[0].name = siteName;
        }
        if (!utilitiesController.isStringNullUndefinedOrEmpty(siteDescription)) {
            doc[0].description = siteDescription;
        }
        return doc[0].save();
    }).then(docUpdate => {
        utilitiesController.returnResponse(res, HTTPStatus.OK, true, docUpdate);
    }).catch(err => {
        utilitiesController.returnErrorToClient(res, err);
    });
};

exports.deleteSite = function (req, res) {
    if (!(utilitiesController.isStringNullUndefinedOrEmpty(req.params.siteId))) {
        const siteId = req.params.siteId;
        const query = spaceUnitSchema.find().where({
            $and: [{siteId: siteId}, {type: spaceUnitTypes.site}]
        });
        let queryExePromise = query.exec();
        queryExePromise.then(doc => {
            utilitiesController.getUniqueObjectValidation(doc, spaceUnitTypes.site, siteId);
            return doc;
        }).then(() => {
            return spaceUnitSchema.remove({siteId: siteId});
        }).then(() => {
            return sensorSchema.remove({siteUnitId: siteId});
        }).then(() => {
            return eventSchema.remove({siteId: siteId});
        }).then(() => {
            return tagSchema.remove({siteId: siteId});
        }).then(() => {
            utilitiesController.returnResponse(res, HTTPStatus.NO_CONTENT, true, responseMessage.ok);
        }).catch(err => {
            utilitiesController.returnErrorToClient(res, err);
        });
    }
    else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};

exports.setSiteLogo = function (req, res) {
    let siteId = req.params.siteId;
    let uploadCode = req.params.uploadCode;
    let fileName = req.params.fileName;
    const query = spaceUnitSchema.find().where({
        $and: [{siteId: siteId}, {type: spaceUnitTypes.site}]
    });
    let queryExePromise = query.exec();
    queryExePromise.then(doc => {
        utilitiesController.getUniqueObjectValidation(doc, spaceUnitTypes.site, siteId);
        return doc;
    }).then(doc => {
        doc[0].logoUrl = config.cloudinaryUrlPrefix + "/" + uploadCode + "/" + fileName;
        return doc[0].save();
    }).then(docUpdate => {
        utilitiesController.returnResponse(res, HTTPStatus.OK, true, docUpdate);
    }).catch(err => {
        utilitiesController.returnErrorToClient(res, err);
    });
};

exports.createBuilding = function (req, res) {
    if (!((utilitiesController.isStringNullUndefinedOrEmpty(req.query.siteId))
        || utilitiesController.isStringNullUndefinedOrEmpty(req.query.width)
        || utilitiesController.isStringNullUndefinedOrEmpty(req.query.height)
        || utilitiesController.isStringNullUndefinedOrEmpty(req.query.buildingName)
        || utilitiesController.isStringNullUndefinedOrEmpty(req.query.buildingDescription))) {
        const siteId = req.query.siteId;
        const width = req.query.width;
        const height = req.query.height;
        const buildingName = req.query.buildingName;
        const buildingDescription = req.query.buildingDescription;
        const query = spaceUnitSchema.find().where({
            $and: [{siteId: siteId}, {type: spaceUnitTypes.site}]
        });
        let queryExePromise = query.exec();
        queryExePromise.then(doc => {
            utilitiesController.getUniqueObjectValidation(doc, spaceUnitTypes.site, siteId)
        }).then(() => {
            const spaceUnitSite = new spaceUnitSchema({
                type: spaceUnitTypes.building,
                name: buildingName,
                description: buildingDescription,
                siteId: siteId,
                parentId: siteId,
                buildingId: -1,
                spaceUnitId: -1,
                floorId: -1,
                accessibility: 1,
                zones: [{
                    topLeftX: 0,
                    topLeftY: 0,
                    widthUnit: width,
                    heightUnit: height
                }]
            });
            return spaceUnitSite.save();
        }).then(doc => {
            doc.buildingId = doc._id;
            doc.spaceUnitId = siteId + "." + doc.buildingId + "." + "0.0";
            return doc.save();
        }).then(docUpdate => {
            utilitiesController.returnResponse(res, HTTPStatus.CREATED, true, docUpdate);
        }).catch(err => {
            utilitiesController.returnErrorToClient(res, err);
        });
    }
    else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};

exports.editBuilding = function (req, res) {
    const buildingName = req.query.buildingName;
    const buildingDescription = req.query.buildingDescription;
    const buildingId = req.params.buildingId;
    const query = spaceUnitSchema.find().where({
        $and: [{buildingId: buildingId}, {type: spaceUnitTypes.building}]
    });
    let queryExePromise = query.exec();
    queryExePromise.then(doc => {
        utilitiesController.getUniqueObjectValidation(doc, spaceUnitTypes.building, buildingId);
        return doc;
    }).then(doc => {
        if (!utilitiesController.isStringNullUndefinedOrEmpty(buildingName)) {
            doc[0].name = buildingName;
        }
        if (!utilitiesController.isStringNullUndefinedOrEmpty(buildingDescription)) {
            doc[0].description = buildingDescription;
        }
        return doc[0].save();
    }).then(docUpdate => {
        utilitiesController.returnResponse(res, HTTPStatus.OK, true, docUpdate);
    }).catch(err => {
        utilitiesController.returnErrorToClient(res, err);
    });
};

exports.deleteBuilding = function (req, res) {
    if (!(utilitiesController.isStringNullUndefinedOrEmpty(req.params.buildingId))) {
        const buildingId = req.params.buildingId;
        const query = spaceUnitSchema.find().where({
            $and: [{buildingId: buildingId}, {type: spaceUnitTypes.building}]
        });
        let queryExePromise = query.exec();
        queryExePromise.then(doc => {
            utilitiesController.getUniqueObjectValidation(doc, spaceUnitTypes.building, buildingId);
            return doc;
        }).then(() => {
            return spaceUnitSchema.remove({buildingId: buildingId});
        }).then(() => {
            utilitiesController.returnResponse(res, HTTPStatus.NO_CONTENT, true, responseMessage.ok);
        }).catch(err => {
            utilitiesController.returnErrorToClient(res, err);
        });
    }
    else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};

exports.getBuildingElevators = function (req, res) {
    if (!(utilitiesController.isStringNullUndefinedOrEmpty(req.params.buildingId))) {
        getObjectsName(res, req.params.buildingId, spaceUnitTypes.elevator)
    }
    else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};

exports.getBuildingStairs = function (req, res) {
    if (!(utilitiesController.isStringNullUndefinedOrEmpty(req.params.buildingId))) {
        getObjectsName(res, req.params.buildingId, spaceUnitTypes.stairs)
    }
    else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};

exports.createFloor = function (req, res) {
    if (!((utilitiesController.isStringNullUndefinedOrEmpty(req.query.buildingId))
            || utilitiesController.isStringNullUndefinedOrEmpty(req.query.floorNumber)
            || utilitiesController.isStringNullUndefinedOrEmpty(req.query.floorWidth)
            || utilitiesController.isStringNullUndefinedOrEmpty(req.query.floorHeight)
        )) {
        const buildingId = req.query.buildingId;
        const floorNumber = req.query.floorNumber;
        const floorWidth = req.query.floorWidth;
        const floorHeight = req.query.floorHeight;
        let buildingObject;
        const query = spaceUnitSchema.find().where({
            $and: [{buildingId: buildingId}, {type: spaceUnitTypes.building}]
        });
        let queryExePromise = query.exec();
        queryExePromise.then(doc => {
            utilitiesController.getUniqueObjectValidation(doc, spaceUnitTypes.building, buildingId);
            return doc;
        }).then(doc => {
            buildingObject = doc[0];
            const query = spaceUnitSchema.find().where({
                $and: [{name: floorNumber}, {type: spaceUnitTypes.floor}, {buildingId: buildingId}]
            });
            return query.exec();
        }).then(doc => {
            if (doc.length !== 0) {
                console.info("Floor number already exist");
                throw {
                    message: responseMessage.floorNumberAlreadyExist,
                    httpStatus: HTTPStatus.CONFLICT
                };
            }
            const floorObject = new spaceUnitSchema({
                type: spaceUnitTypes.floor,
                name: floorNumber,
                siteId: buildingObject.siteId,
                buildingId: buildingObject.buildingId,
                parentId: buildingObject.siteId,
                accessibility: 1,
                zones: [{
                    topLeftX: 0,
                    topLeftY: 0,
                    widthUnit: floorWidth,
                    heightUnit: floorHeight
                }]
            });
            return floorObject.save();
        }).then(doc => {
            doc.floorId = doc._id;
            doc.spaceUnitId = doc.siteId + "." + doc.buildingId + "." + doc.floorId + ".0";
            return doc.save()
        }).then(docUpdate => {
            utilitiesController.returnResponse(res, HTTPStatus.CREATED, true, docUpdate);
        }).catch(err => {
            utilitiesController.returnErrorToClient(res, err);
        });
    }
    else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};

/*
 This method remove objects of floor and replace them with new objects.
 This function also do a binding of elevators and stairs in binding of floor.
 */
exports.setFloorObjects = function (req, res) {
    if (!(utilitiesController.isStringNullUndefinedOrEmpty(req.query.floorId))) {
        const floorSpaceUnits = JSON.parse(req.rawBody);
        const floorId = req.query.floorId;
        let floorObject;
        let buildingId;
        const query = spaceUnitSchema.find().where({
            $and: [{floorId: floorId}, {type: spaceUnitTypes.floor}]
        });
        let queryExePromise = query.exec();
        let tempObjectsArray = [];
        queryExePromise.then(doc => {
            utilitiesController.getUniqueObjectValidation(doc, spaceUnitTypes.building, floorId);
            floorObject = doc[0];
        }).then(() => {
            return spaceUnitSchema.deleteMany({parentId: floorId})
        }).then(() => {
            const siteId = floorObject.siteId;
            buildingId = floorObject.buildingId;
            const prefixSpaceUnitId = siteId + "." + buildingId + "." + floorId + ".";
            floorSpaceUnits.forEach(function (spaceUnit) {
                spaceUnit.siteId = siteId;
                spaceUnit.buildingId = buildingId;
                spaceUnit.floorId = floorId;
                spaceUnit.parentId = floorId;
                spaceUnit.id = clearSpaceUnitIdFromPrefix(spaceUnit.id);
                spaceUnit.spaceUnitId = prefixSpaceUnitId + spaceUnit.id;
                spaceUnit.entrance.forEach(function (entrance) {
                    entrance.neighborId = prefixSpaceUnitId + clearSpaceUnitIdFromPrefix(entrance.neighborId);
                });
            });
            return spaceUnitSchema.insertMany(floorSpaceUnits);
        }).then(() => {
            let promiseArray = [];
            promiseArray.push(getPromiseBuildingStairs(buildingId));
            promiseArray.push(getPromiseBuildingElevator(buildingId));
            return Promise.all(promiseArray);
        }).then(doc => {
            let stairs = bindBuildingFloorsObjects(doc[0]);
            let elevators = bindBuildingFloorsObjects(doc[1]);
            tempObjectsArray = stairs.concat(elevators);
            let promiseArray = [];
            promiseArray.push(deleteElevatorObjectsPromise(buildingId));
            promiseArray.push(deleteStairObjectsPromise(buildingId));
            return Promise.all(promiseArray);
        }).then(() => {
            let promiseArray = [];
            tempObjectsArray.forEach(object => {
                promiseArray.push(object.save());
            });
            return Promise.all(promiseArray);
        }).then(() => {
            utilitiesController.returnResponse(res, HTTPStatus.OK, true, responseMessage.ok);
        }).catch(err => {
            console.error("Critical! Error on setFloorObjects!");
            utilitiesController.returnErrorToClient(res, err);
        });
    }
    else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};

exports.editFloor = function (req, res) {
    const floorNumber = req.query.floorNumber;
    const floorId = req.params.floorId;
    let buildingId;
    let floorObject;
    const query = spaceUnitSchema.find().where({
        $and: [{floorId: floorId}, {type: spaceUnitTypes.floor}]
    });
    let queryExePromise = query.exec();
    queryExePromise.then(doc => {
        utilitiesController.getUniqueObjectValidation(doc, spaceUnitTypes.floor, floorId);
        buildingId = doc[0].buildingId;

        return doc;
    }).then(doc => {
        if (!utilitiesController.isStringNullUndefinedOrEmpty(floorNumber)) {
            doc[0].name = floorNumber;
        }
        floorObject = doc[0];
        const query = spaceUnitSchema.find().where({
            $and: [{name: floorNumber}, {type: spaceUnitTypes.floor}, {buildingId: buildingId}]
        });
        return query.exec();
    }).then(doc => {
        if (doc.length !== 0) {
            console.error("Floor number already exist");
            throw {
                message: responseMessage.floorNumberAlreadyExist,
                httpStatus: HTTPStatus.CONFLICT
            };
        }
        return floorObject.save();
    }).then(docUpdate => {
        utilitiesController.returnResponse(res, HTTPStatus.OK, true, docUpdate);
    }).catch(err => {
        utilitiesController.returnErrorToClient(res, err);
    });
};

exports.deleteFloor = function (req, res) {
    if (!(utilitiesController.isStringNullUndefinedOrEmpty(req.params.floorId))) {
        const floorId = req.params.floorId;
        const query = spaceUnitSchema.find().where({
            $and: [{floorId: floorId}, {type: spaceUnitTypes.floor}]
        });
        const queryExePromise = query.exec();
        queryExePromise.then(doc => {
            utilitiesController.getUniqueObjectValidation(doc, spaceUnitTypes.floor, floorId);
            return doc;
        }).then(() => {
            return spaceUnitSchema.remove({floorId: floorId});
        }).then(() => {
            utilitiesController.returnResponse(res, HTTPStatus.NO_CONTENT, true, responseMessage.ok);
        }).catch(err => {
            utilitiesController.returnErrorToClient(res, err);
        });
    }
    else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};

exports.createSensor = function (req, res) {
    if (!((utilitiesController.isStringNullUndefinedOrEmpty(req.query.identifier))
            || utilitiesController.isStringNullUndefinedOrEmpty(req.query.uuid)
            || utilitiesController.isStringNullUndefinedOrEmpty(req.query.spaceUnitId)
            || utilitiesController.isStringNullUndefinedOrEmpty(req.query.minor)
            || utilitiesController.isStringNullUndefinedOrEmpty(req.query.major)
            || utilitiesController.isStringNullUndefinedOrEmpty(req.query.coordY)
            || utilitiesController.isStringNullUndefinedOrEmpty(req.query.coordX)
        )) {
        const identifier = req.query.identifier;
        const uuid = req.query.uuid;
        const spaceUnitId = req.query.spaceUnitId;
        const minor = req.query.minor;
        const major = req.query.major;
        const coordY = req.query.coordY;
        const coordX = req.query.coordX;
        const siteId = getSiteIdFromSpaceUnitId(spaceUnitId);
        const floorUnitId = getFloorUnitIdIdFromSpaceUnitId(spaceUnitId);
        const query = spaceUnitSchema.find().where({
            spaceUnitId: spaceUnitId
        });
        query.then(doc => {
            validateSensorSpaceUnit(doc, spaceUnitId, siteId);
        }).then(() => {
            const sensor = new sensorSchema(
                {
                    identifier: identifier,
                    uuid: uuid,
                    spaceUnitId: spaceUnitId,
                    minor: minor,
                    major: major,
                    siteUnitId: siteId,
                    floorUnitId: floorUnitId,
                    coordX: coordX,
                    coordY: coordY
                });
            return sensor.save();
        }).then(doc => {
            utilitiesController.returnResponse(res, HTTPStatus.CREATED, true, doc);
        }).catch(err => {
            utilitiesController.returnErrorToClient(res, err);
        });
    }
    else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};

exports.editSensor = function (req, res) {
    const sensorId = req.params.sensorId;
    const identifier = req.query.identifier;
    const uuid = req.query.uuid;
    const spaceUnitId = req.query.spaceUnitId;
    const minor = req.query.minor;
    const major = req.query.major;
    let updateSpaceUnitId = false;
    let siteId = undefined;
    const query = sensorSchema.find().where({
        _id: sensorId
    });
    let queryExePromise = query.exec();
    queryExePromise.then(doc => {
        validateSensorFromDb(doc, sensorId);
        return doc;
    }).then(doc => {
        if (!utilitiesController.isStringNullUndefinedOrEmpty(identifier)) {
            doc[0].identifier = identifier;
        }
        if (!utilitiesController.isStringNullUndefinedOrEmpty(uuid)) {
            doc[0].description = uuid;
        }
        if (!utilitiesController.isStringNullUndefinedOrEmpty(minor)) {
            doc[0].minor = minor;
        }
        if (!utilitiesController.isStringNullUndefinedOrEmpty(minor)) {
            doc[0].major = major;
        }
        if (!utilitiesController.isStringNullUndefinedOrEmpty(spaceUnitId)) {
            siteId = getSiteIdFromSpaceUnitId(spaceUnitId);
            updateSpaceUnitId = true;
            doc[0].spaceUnitId = spaceUnitId;
            doc[0].siteUnitId = siteId;
            const query = spaceUnitSchema.find().where({
                spaceUnitId: spaceUnitId
            });
            return query.exec();
        } else {
            return doc[0].save();
        }
    }).then(doc => {
        if (updateSpaceUnitId) {
            validateSensorSpaceUnit(doc, spaceUnitId, siteId);
        }
        return doc;
    }).then(doc => {
        utilitiesController.returnResponse(res, HTTPStatus.OK, true, doc);
    }).catch(err => {
        utilitiesController.returnErrorToClient(res, err);
    });
};

exports.deleteSensor = function (req, res) {
    if (!(utilitiesController.isStringNullUndefinedOrEmpty(req.params.sensorId))) {
        const sensorId = req.params.sensorId;
        const query = sensorSchema.find().where({
            $and: [{_id: sensorId}]
        });
        let queryExePromise = query.exec();
        queryExePromise.then(doc => {
            validateSensorFromDb(doc, sensorId);
            return doc;
        }).then(() => {
            return sensorSchema.remove({_id: sensorId});
        }).then(() => {
            utilitiesController.returnResponse(res, HTTPStatus.NO_CONTENT, true, responseMessage.ok);
        }).catch(err => {
            utilitiesController.returnErrorToClient(res, err);
        });
    }
    else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};

exports.createTag = function (req, res) {
    if (!((utilitiesController.isStringNullUndefinedOrEmpty(req.query.siteId))
            || utilitiesController.isStringNullUndefinedOrEmpty(req.query.name)
            || utilitiesController.isStringNullUndefinedOrEmpty(req.query.tagType)
        )) {
        const siteId = req.query.siteId;
        const name = req.query.name;
        const tagType = req.query.tagType;
        const query = spaceUnitSchema.find().where({
            $and: [{siteId: siteId}, {type: spaceUnitTypes.site}]
        });
        query.then(doc => {
            utilitiesController.getUniqueObjectValidation(doc, spaceUnitTypes.site, siteId);
            return doc;
        }).then(() => {
            const tag = new tagSchema(
                {
                    siteId: siteId,
                    name: name,
                    tagType: tagType,
                });
            return tag.save();
        }).then(doc => {
            utilitiesController.returnResponse(res, HTTPStatus.CREATED, true, doc);
        }).catch(err => {
            utilitiesController.returnErrorToClient(res, err);
        });
    }
    else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};

exports.editTag = function (req, res) {
    utilitiesController.returnResponse(res, HTTPStatus.NOT_IMPLEMENTED, true, responseMessage.notImplement);
};

exports.deleteTag = function (req, res) {
    if (!(utilitiesController.isStringNullUndefinedOrEmpty(req.params.tagId))) {
        const tagId = req.params.tagId;
        const query = tagSchema.find().where({
            $and: [{_id: tagId}]
        });
        let queryExePromise = query.exec();
        queryExePromise.then(doc => {
            validateTagFromDb(doc, tagId);
            return doc;
        }).then(() => {
            return tagSchema.remove({_id: tagId});
        }).then(() => {
            utilitiesController.returnResponse(res, HTTPStatus.NO_CONTENT, true, responseMessage.ok);
        }).catch(err => {
            utilitiesController.returnErrorToClient(res, err);
        });
    }
    else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};


exports.addSpaceUnitsToTag = function (req, res) {
    if (!(utilitiesController.isStringNullUndefinedOrEmpty(req.params.tagId))
        || utilitiesController.isStringNullUndefinedOrEmpty(req.query.spaceUnitsIds)) {
        let tagId = req.params.tagId;
        const query = tagSchema.find().where({
            _id: tagId
        });
        let queryExePromise = query.exec();
        let spaceUnitsObjectsIds = req.query.spaceUnitsIds.split(',');
        queryExePromise.then(doc => {
            validateTagFromDb(doc, tagId);
            return doc;
        }).then(doc => {
            spaceUnitsObjectsIds.forEach(spaceUnitId => {
                doc[0].roomsIds.push(spaceUnitId);
            });
            doc[0].roomsIds = Array.from(new Set(doc[0].roomsIds));
            return doc[0].save();
        }).then(doc => {
            utilitiesController.returnResponse(res, HTTPStatus.OK, true, doc)
        }).catch(err => {
            utilitiesController.returnErrorToClient(res, err);
        });
    }
    else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};

exports.removeSpaceUnitsFromTag = function (req, res) {
    if (!(utilitiesController.isStringNullUndefinedOrEmpty(req.params.tagId))
        || utilitiesController.isStringNullUndefinedOrEmpty(req.query.spaceUnitsIds)) {
        let tagId = req.params.tagId;
        const query = tagSchema.find().where({
            _id: tagId
        });
        let queryExePromise = query.exec();
        let spaceUnitsObjectsIds = req.query.spaceUnitsIds.split(',');
        queryExePromise.then(doc => {
            validateTagFromDb(doc, tagId);
            return doc;
        }).then(doc => {
            spaceUnitsObjectsIds.forEach(spaceUnitId => {
                doc[0].roomsIds = doc[0].roomsIds.filter(function (id) {
                    return id !== spaceUnitId;
                })
            });
            return doc[0].save();
        }).then(doc => {
            utilitiesController.returnResponse(res, HTTPStatus.OK, true, doc)
        }).catch(err => {
            utilitiesController.returnErrorToClient(res, err);
        });
    }
    else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};



/*
 This method connect between elevators or stairs in different floors for navigation algorithm
 */
function bindBuildingFloorsObjects(objects) {
    let result = [];
    clearObjectsBeforeBind(objects);
    objects.forEach(object => {
        objects.forEach(objectToCompare => {
            if (object.spaceUnitId !== objectToCompare.spaceUnitId) {
                let approachable;
                if (object.type === spaceUnitTypes.stairs) {
                    approachable = 1;
                }
                else {
                    approachable = 0;
                }
                object.entrance.push({
                    neighborId: objectToCompare.spaceUnitId,
                    direction: "none",
                    approachable: approachable
                });
            }
        });
        result.push(new spaceUnitSchema({
            type: object.type,
            name: object.name,
            siteId: object.siteId,
            buildingId: object.buildingId,
            floorId: object.floorId,
            parentId: object.floorId,
            spaceUnitId: object.spaceUnitId,
            entrance: object.entrance,
            zones: object.zones
        }));

    });
    return result;
}


function getPromiseBuildingStairs(buildingId) {
    return spaceUnitSchema.find().where({
        $and: [{buildingId: buildingId}, {type: spaceUnitTypes.elevator}]
    });
}

function getPromiseBuildingElevator(buildingId) {
    return spaceUnitSchema.find().where({
        $and: [{buildingId: buildingId}, {type: spaceUnitTypes.stairs}]
    });
}

function deleteElevatorObjectsPromise(buildingId) {
    return spaceUnitSchema.deleteMany({$and: [{buildingId: buildingId}, {type: spaceUnitTypes.elevator}]});
}

function deleteStairObjectsPromise(buildingId) {
    return spaceUnitSchema.deleteMany({$and: [{buildingId: buildingId}, {type: spaceUnitTypes.stairs}]});
}


/*
 This method cleaning previous connections between stairs or elevators
 */
function clearObjectsBeforeBind(objects) {
    let objectEntrance = null;
    let objectEntranceAfterClear = null;
    objects.forEach(object => {
        objectEntranceAfterClear = [];
        objectEntrance = object.entrance;
        objectEntrance.forEach(entrance => {
            if (isSpaceUnitIdBelongToCorridor(entrance.neighborId)) {
                objectEntranceAfterClear.push(entrance);
            }
        });
        object.entrance = objectEntranceAfterClear;
    })
}

function isSpaceUnitIdBelongToCorridor(spaceUnitId) {
    return spaceUnitId.search("x") !== -1
}

function clearSpaceUnitIdFromPrefix(spaceUnitId) {
    if (typeof spaceUnitId === "number") {
        spaceUnitId = spaceUnitId.toString();
    }
    spaceUnitId = spaceUnitId.toString();
    let result;
    let index = spaceUnitId.indexOf(".");
    if (index === -1) {
        result = spaceUnitId;
    }
    else {
        result = spaceUnitId.split(".")[3];
    }
    return result;
}

// function return objects names of given type
function getObjectsName(res, buildingId, type) {
    const query = spaceUnitSchema.find().where({
        $and: [{buildingId: buildingId}, {type: spaceUnitTypes.building}]
    });
    let queryExePromise = query.exec();
    queryExePromise.then(doc => {
        utilitiesController.getUniqueObjectValidation(doc, spaceUnitTypes.building, buildingId);
        return doc;
    }).then(() => {
        const query = spaceUnitSchema.find().where({
            $and: [{buildingId: buildingId}, {type: type}]
        });
        return query.exec();
    }).then(doc => {
        let names = [];
        doc.forEach(function (object) {
            let flag = names.find(function (name) { //check if name already on array names
                return name.name === object.name;
            });
            if (flag === undefined) {
                names.push({name: object.name});
            }
        });
        utilitiesController.returnResponse(res, HTTPStatus.OK, true, names);
    }).catch(err => {
        utilitiesController.returnErrorToClient(res, err);
    });
}

function getSiteIdFromSpaceUnitId(spaceUnitId) {
    return Number(spaceUnitId.split('.')[0]);
}

function getFloorUnitIdIdFromSpaceUnitId(spaceUnitId) {
    return Number(spaceUnitId.split('.')[2]);
}
function validateSensorFromDb(sensorDoc, sensorId) {
    if (sensorDoc.length === 0) {
        throw {
            message: responseMessage.sensorNotFound,
            httpStatus: HTTPStatus.NOT_FOUND
        };
    }
    else if (sensorDoc.length > 1) {
        console.error("Something wrong.Sensor _id is not unique:" + sensorId);
        throw {
            message: responseMessage.serverError,
            httpStatus: HTTPStatus.INTERNAL_SERVER_ERROR
        };
    }
}

function validateSensorSpaceUnit(doc, spaceUnitId, siteId) {
    utilitiesController.getUniqueObjectValidation(doc, spaceUnitTypes.room, NaN);
    if (doc[0].siteId !== siteId) {
        console.error("Sensor spaceUnitId  " + spaceUnitId + "  not belong to site " + siteId);
        throw {
            message: responseMessage.illegalArgument,
            httpStatus: HTTPStatus.BAD_REQUEST
        };
    }
}

function validateTagFromDb(doc, tagId) {
    utilitiesController.getUniqueObjectValidation(doc, "tag", tagId);
}