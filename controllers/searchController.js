'use strict';
console.info("file searchController.js loading");
const HTTPStatus = require('http-status');
const intersect = require('intersect');
const utilitiesController = require('./utilitiesController');
const responseMessage = require('../responseMessage.json');
const tagSchema = require('./../db/tagSchema');
const spaceUnitSchema = require('./../db/spaceUnitSchema');
const eventSchema = require('./../db/eventSchema');
const spaceUnitTypes = require('../spaceUnitTypes.json');

exports.search = function (req, res) {
    let searchRequest;
    let requestDataError = false;
    try {
        searchRequest = JSON.parse(req.rawBody);
        if (typeof searchRequest.siteId === "undefined") {
            console.error("searchRequest.siteId is undefined");
            requestDataError = true;
        }
    }
    catch (e) {
        requestDataError = true;
    }
    if (!requestDataError) {
        let promiseArray = [];
        if (typeof searchRequest.roomName !== "undefined") {
            findSpaceUnitsById(res, searchRequest.roomName)
        }
        else {
            if (typeof searchRequest.tags !== "undefined" && searchRequest.tags.length > 0) {
                promiseArray.push(searchByTag(searchRequest));
            }
            if (typeof searchRequest.events !== "undefined" && typeof searchRequest.events.name !== "undefined") {
                promiseArray.push(searchByEvents(searchRequest));
            }
            if (typeof searchRequest.freeRooms !== "undefined" && typeof searchRequest.freeRooms.startTime !== "undefined") {
                promiseArray.push(searchFreeRooms(searchRequest));
            }
            Promise.all(promiseArray).then(doc => {
                let roomsIds = doc[0];
                for (let i = 1; i < doc.length; i++) {
                    roomsIds = intersect(roomsIds, doc[i]);
                }
                return getSpaceUnitsByIds(roomsIds);
            }).then(doc => {
                let result = filterResultByPhysical(searchRequest, doc);
                if (typeof searchRequest.accessibility !== "undefined") {
                    result = filterResultByAccessibility(result, searchRequest.accessibility);
                }
                utilitiesController.returnResponse(res, HTTPStatus.OK, true, result);
            }).catch(err => {
                utilitiesController.returnErrorToClient(res, err);
            });
        }
    } else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};

function searchFreeRooms(searchRequest) {
    let search = searchRequest.freeRooms;
    let queryEventsPromise = eventSchema.find({
        $and: [
            {siteId: searchRequest.siteId}, {dateTimeStart: {$gte: new Date(search.startTime)}}, {dateTimeEnd: {$lte: new Date(search.endTime)}}
        ]
    });
    let spaceUnitsPromise = spaceUnitSchema.find({
        $and: [{siteId: searchRequest.siteId}, {type: spaceUnitTypes.room}]
    });
    let promise = Promise.all([queryEventsPromise, spaceUnitsPromise]);
    return promise.then(doc => {
        return getFreeUnitsIds(doc);
    }).catch(err => {
        console.error(err);
        throw {
            message: responseMessage.illegalArgument,
            httpStatus: HTTPStatus.BAD_REQUEST
        };
    });
}

function getFreeUnitsIds(doc) {
    let events = doc[0];
    let spaceUnits = doc[1];
    let freeSpaceUnits = [];
    let eventsArrayLength = events.length;
    spaceUnits.forEach(spaceUnit => {
        let unitFreeFlag = true;
        for (let i = 0; i < eventsArrayLength; i++) {
            if (events[i].spaceUnitIds.indexOf(spaceUnit.spaceUnitId) !== -1) {
                unitFreeFlag = false;
            }
        }
        if (unitFreeFlag) {
            freeSpaceUnits.push(spaceUnit);
        }
    });
    let result = [];
    freeSpaceUnits.forEach(spaceUnit => {
        result.push(spaceUnit.spaceUnitId);
    });
    return result
}

function searchByEvents(searchRequest) {
    let startTime = new Date(searchRequest.events.startTime);
    let endTime = new Date(searchRequest.events.endTime);
    let eventName = searchRequest.events.name;
    let queryEventsPromise = eventSchema.find({
        $and: [
            {siteId: searchRequest.siteId}, {name: eventName}, {dateTimeStart: {$gte: startTime}}, {dateTimeEnd: {$lte: endTime}}
        ]
    });
    return queryEventsPromise.then(doc => {
        return getSpaceUnitsIdsFromEvents(doc);
    }).catch(err => {
        console.error(err);
        throw {
            message: responseMessage.illegalArgument,
            httpStatus: HTTPStatus.BAD_REQUEST
        };
    });
}

function searchByTag(searchRequest) {
    let queryTags = prepareTagsForSearch(searchRequest);
    const query = tagSchema.find().where({
        $or: queryTags
    });
    let queryTagsPromise = query.exec();
    return queryTagsPromise.then(doc => {
        return getSpaceUnitsIdsFromTag(doc)
    }).catch(err => {
        console.error(err);
        throw {
            message: responseMessage.illegalArgument,
            httpStatus: HTTPStatus.BAD_REQUEST
        };
    });
}

function findSpaceUnitsById(res, roomName) {
    const query = spaceUnitSchema.find().where(
        {name: roomName}
    );
    utilitiesController.getObjectsFromDb(res, query);
}

function filterResultByPhysical(searchRequest, data) {
    let result = data;
    if (searchRequest.physical !== undefined) {
        if (searchRequest.physical.width !== undefined) {
            result = filterResultByPhysicalKey(result, "widthUnit", searchRequest.physical.width);
        }
        if (searchRequest.physical.height !== undefined) {
            result = filterResultByPhysicalKey(result, "heightUnit", searchRequest.physical.height);
        }
        if (searchRequest.physical.capacity !== undefined) {
            result = filterResultByPhysicalKey(result, "capacity", searchRequest.physical.capacity);
        }
    }
    return result
}

function filterResultByPhysicalKey(data, key, value) {
    let result = [];
    data.forEach(spaceUnit => {
        if (key !== "capacity") {
            if (value <= spaceUnit.zones[0][key]) {
                result.push(spaceUnit)
            }
        }
        else {
            if (value <= spaceUnit.capacity) {
                result.push(spaceUnit)
            }
        }
    });
    return result;
}

function filterResultByAccessibility(data, accessibility) {
    let result = [];
    data.forEach(spaceUnit => {
        if (accessibility >= spaceUnit.accessibility) {
            result.push(spaceUnit);
        }
    });
    return result;
}

function prepareTagsForSearch(searchRequest) {
    let tagsForSearch = [];
    searchRequest.tags.forEach(tags => {
        tags.tags.forEach(tag => {
            tagsForSearch.push({
                type: tags.type,
                name: tag,
                siteId: searchRequest.siteId
            })
        })
    });
    const queryTags = [];
    tagsForSearch.forEach(tag => {
        queryTags.push({
            name: tag.name,
            tagType: tag.type,
            siteId: searchRequest.siteId
        });
    });
    return queryTags;
}

function getSpaceUnitsIdsFromTag(tags) {
    if (tags.length === 0) {
        return [];
    }
    let roomsIdsArray = tags[0].roomsIds;
    for (let i = 1; i < tags.length; i++) {
        roomsIdsArray = intersect(roomsIdsArray, tags[i].roomsIds);
    }
    let roomsForSearch = [];
    roomsIdsArray.forEach(roomId => {
        roomsForSearch.push(roomId);
    });
    return roomsForSearch;
}

function getSpaceUnitsIdsFromEvents(events) {
    if (events.length === 0) {
        return [];
    }
    let roomsIdsArray = events[0].spaceUnitIds;
    for (let i = 1; i < events.length; i++) {
        roomsIdsArray = roomsIdsArray.concat(events[i].spaceUnitIds);
    }
    let roomsForSearch = [];
    roomsIdsArray.forEach(roomId => {
        roomsForSearch.push(roomId);
    });
    return roomsForSearch;
}

function getSpaceUnitsByIds(roomsForSearch) {
    let roomsIdsArray = [];
    roomsForSearch.forEach(roomId => {
        roomsIdsArray.push({
            spaceUnitId: roomId
        });
    });
    const query = spaceUnitSchema.find().where({
        $or: roomsIdsArray
    });
    if (roomsIdsArray.length === 0) {
        return [];
    } else {
        return query.exec();
    }
}