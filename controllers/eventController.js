'use strict';
console.info("file eventController.js loading");
const HTTPStatus = require('http-status');
const eventSchema = require('./../db/eventSchema');
const utilitiesController = require('./utilitiesController');
const responseMessage = require('../responseMessage.json');
const spaceUnitTypes = require('../spaceUnitTypes.json');
const spaceUnitSchema = require('./../db/spaceUnitSchema');

exports.createEvent = function (req, res) {
    if (!((utilitiesController.isStringNullUndefinedOrEmpty(req.query.siteId))
            || utilitiesController.isStringNullUndefinedOrEmpty(req.query.name)
            || utilitiesController.isStringNullUndefinedOrEmpty(req.query.dateTimeStart)
            || utilitiesController.isStringNullUndefinedOrEmpty(req.query.dateTimeEnd)
            || utilitiesController.isStringNullUndefinedOrEmpty(req.query.usersIds)
            || utilitiesController.isStringNullUndefinedOrEmpty(req.query.spaceUnitsIds)
        )) {
        const siteId = req.query.siteId;
        const name = req.query.name;
        const description = req.query.description;
        const dateTimeStart = req.query.dateTimeStart;
        const dateTimeEnd = req.query.dateTimeEnd;
        const persistence = req.query.persistence;
        let startTime = new Date(dateTimeStart);
        let endTime = new Date(dateTimeEnd);
        let usersIds = req.query.usersIds.split(',').map(Number);
        let spaceUnitsIds = req.query.spaceUnitsIds.split(',');
        const query = spaceUnitSchema.find().where({
            $and: [{siteId: siteId}, {type: spaceUnitTypes.site}]
        });
        const queryExePromise = query.exec();
        queryExePromise.then(doc => {
            utilitiesController.getUniqueObjectValidation(doc, spaceUnitTypes.site, siteId);
            return doc;
        }).then(() => {
            const event = new eventSchema(
                {
                    name: name,
                    siteId: siteId,
                    description: description,
                    spaceUnitIds: spaceUnitsIds,
                    usersIds: usersIds,
                    dateTimeStart: startTime,
                    dateTimeEnd: endTime,
                    persistence: persistence
                });
            return event.save();
        }).then(doc => {
            utilitiesController.returnResponse(res, HTTPStatus.CREATED, true, doc);
        }).catch(err => {
            utilitiesController.returnErrorToClient(res, err);
        });
    }
    else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};

exports.addSpaceUnitsToEvent = function (req, res) {
    if (!(utilitiesController.isStringNullUndefinedOrEmpty(req.params.eventId))
        || utilitiesController.isStringNullUndefinedOrEmpty(req.query.spaceUnitsIds)) {
        let eventId = req.params.eventId;
        const query = eventSchema.find().where({
            _id: eventId
        });
        let queryExePromise = query.exec();
        let spaceUnitsObjectsIds = req.query.spaceUnitsIds.split(',');
        queryExePromise.then(doc => {
            validateEventFromDb(doc, eventId);
            return doc;
        }).then(doc => {
            spaceUnitsObjectsIds.forEach(spaceUnitId => {
                doc[0].spaceUnitIds.push(spaceUnitId);
            });
            doc[0].spaceUnitIds = Array.from(new Set(doc[0].spaceUnitIds));
            return doc[0].save();
        }).then(doc => {
            utilitiesController.returnResponse(res, HTTPStatus.OK, true, doc)
        }).catch(err => {
            utilitiesController.returnErrorToClient(res, err);
        });
    }
    else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};

exports.removeSpaceUnitsFromEvent = function (req, res) {
    if (!(utilitiesController.isStringNullUndefinedOrEmpty(req.params.eventId))
        || utilitiesController.isStringNullUndefinedOrEmpty(req.query.spaceUnitsIds)) {
        let eventId = req.params.eventId;
        const query = eventSchema.find().where({
            _id: eventId
        });
        let queryExePromise = query.exec();
        let spaceUnitsObjectsIds = req.query.spaceUnitsIds.split(',');
        queryExePromise.then(doc => {
            validateEventFromDb(doc, eventId);
            return doc;
        }).then(doc => {
            spaceUnitsObjectsIds.forEach(spaceUnitId => {
                doc[0].spaceUnitIds = doc[0].spaceUnitIds.filter(function (id) {
                    return id !== spaceUnitId;
                })
            });
            return doc[0].save();
        }).then(doc => {
            utilitiesController.returnResponse(res, HTTPStatus.OK, true, doc)
        }).catch(err => {
            utilitiesController.returnErrorToClient(res, err);
        });
    }
    else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};

exports.getEventsOfSpaceUnit = function (req, res) {
    const spaceUnitId = req.params.spaceUnitId;
    let query = getEventsOfSpaceUnitPromise(spaceUnitId, req.params.dateTimeStart, req.params.dateTimeEnd);
    const queryExePromise = query.exec();
    queryExePromise.then(doc => {
        utilitiesController.returnResponse(res, HTTPStatus.OK, true, doc);
    }).catch(err => {
        utilitiesController.returnErrorToClient(res, err);
    });
};

exports.getEventsOfSpaceUnitPromise = function (spaceUnitId, dateTimeStart, dateTimeEnd) {
    return getEventsOfSpaceUnitPromise(spaceUnitId, dateTimeStart, dateTimeEnd)
};

function getEventsOfSpaceUnitPromise(spaceUnitId, dateTimeStart, dateTimeEnd) {
    return eventSchema.find({
        $and: [
            {spaceUnitIds: {$in: [spaceUnitId]}},
            {dateTimeStart: {$gte: dateTimeStart}},
            {dateTimeEnd: {$lte: dateTimeEnd}}
        ]
    });
}

exports.getEventRoomsByEventName = function (req, res) {
    if (!(utilitiesController.isStringNullUndefinedOrEmpty(req.params.name))
        || utilitiesController.isStringNullUndefinedOrEmpty(req.params.startTime)
        || utilitiesController.isStringNullUndefinedOrEmpty(req.params.endTime)) {
        let startTime = new Date(req.params.startTime);
        let endTime = new Date(req.params.endTime);
        let eventName = req.params.name;
        let queryExePromise = eventSchema.find({
            $and: [
                {name: eventName}, {dateTimeStart: {$gte: startTime}}, {dateTimeEnd: {$gte: endTime}}
            ]
        });
        queryExePromise.then(doc => {
            utilitiesController.returnResponse(res, HTTPStatus.OK, true, doc);
        }).catch(err => {
            utilitiesController.returnErrorToClient(res, err);
        });
    } else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};

exports.getEventRoomsByEventName = function (req, res) {
    if (!(utilitiesController.isStringNullUndefinedOrEmpty(req.params.name))
        || utilitiesController.isStringNullUndefinedOrEmpty(req.params.startTime)
        || utilitiesController.isStringNullUndefinedOrEmpty(req.params.endTime)) {
        let startTime = new Date(req.params.startTime);
        let endTime = new Date(req.params.endTime);
        let eventName = req.params.name;
        let queryExePromise = eventSchema.find({
            $and: [
                {name: eventName}, {dateTimeStart: {$gte: startTime}}, {dateTimeEnd: {$lte: endTime}}
            ]
        });
        queryExePromise.then(doc => {
            let roomsForSearch = [];
            doc.forEach(event => {
                event.spaceUnitIds.forEach(spaceUnitId => {
                    roomsForSearch.push({
                        spaceUnitId: spaceUnitId
                    });
                });
            });
            const query = spaceUnitSchema.find().where({
                $or: roomsForSearch
            });
            return query.exec();
        }).then(doc => {
            utilitiesController.returnResponse(res, HTTPStatus.OK, true, doc);
        }).catch(err => {
            utilitiesController.returnErrorToClient(res, err);
        });
    } else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};

exports.getEventsForLoggedInUser = function (req, res) {
    if (res.locals.user !== null) {
        let queryExePromise = eventSchema.find({
            $and: [
                {siteId: req.params.siteId},
                {usersIds: {$in: [res.locals.user._id]}},
                {dateTimeStart: {$gte: req.params.startTime}},
                {dateTimeEnd: {$lte: req.params.endTime}}
            ]
        });
        queryExePromise.then(doc => {
            utilitiesController.returnResponse(res, HTTPStatus.OK, true, doc);
        }).catch(err => {
            utilitiesController.returnErrorToClient(res, err);
        });
    }
    else {
        utilitiesController.returnResponse(res, HTTPStatus.UNAUTHORIZED, false, responseMessage.Unauthorized);
    }
};

function validateEventFromDb(doc, eventId) {
    utilitiesController.getUniqueObjectValidation(doc, "event", eventId);
}
