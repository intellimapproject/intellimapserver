'use strict';
console.info("file mapConsumerController.js loading");
const HTTPStatus = require('http-status');
const utilitiesController = require('./utilitiesController');
const eventController = require('./eventController');
const spaceUnitSchema = require('./../db/spaceUnitSchema');
const sensorSchema = require('./../db/sensorSchema');
const responseMessage = require('../responseMessage.json');
const spaceUnitTypes = require('../spaceUnitTypes.json');
const tagSchema = require('./../db/tagSchema');
const eventSchema = require('./../db/eventSchema');

let floorArrayCache = [];
/*cache for floor (array of spaceUnits that represent floor) */
let siteInformationForClientCache = [];
/* cache for /api/v1/mapConsumer/site/:siteId/siteInformationForClient call */


exports.getAllSitesData = function (req, res) {
    let promiseArray = [];
    promiseArray.push(getAllSites());
    promiseArray.push(getAllSitesSensors());
    Promise.all(promiseArray).then(doc => {
        let result = {};
        result.sites = doc[0];
        result.sensors = doc[1];
        utilitiesController.returnResponse(res, HTTPStatus.OK, true, result);
    }).catch(err => {
        utilitiesController.returnErrorToClient(res, err);
    });
};

exports.getAllSites = function (req, res) {
    const query = spaceUnitSchema.find().where({type: spaceUnitTypes.site});
    utilitiesController.getObjectsFromDb(res, query);
};

exports.getAllSitesSensors = function (req, res) {
    const query = sensorSchema.find({});
    utilitiesController.getObjectsFromDb(res, query);
};


exports.getSite = function (req, res) {
    const siteId = req.params.siteId;
    const query = spaceUnitSchema.find().where({
        $and: [{siteId: siteId}, {type: spaceUnitTypes.site}]
    });
    utilitiesController.getObjectFromDb(res, query, siteId, spaceUnitTypes.site);
};

exports.getSpaceUnitsByIds = function (req, res) {
    if (!( utilitiesController.isStringNullUndefinedOrEmpty(req.query.spacesUnitsIds))) {
        let spacesUnitsIds = req.query.spacesUnitsIds.split(',');
        let spacesUnitsIdsArray = [];
        spacesUnitsIds.forEach(spaceUnitId => {
            spacesUnitsIdsArray.push({
                spaceUnitId: spaceUnitId
            });
        });
        const query = spaceUnitSchema.find().where({
            $or: spacesUnitsIdsArray
        });
        let queryExePromise = query.exec();
        queryExePromise.then(doc => {
            utilitiesController.returnResponse(res, HTTPStatus.OK, true, doc);
        });
    }
    else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};


exports.getSiteTags = function (req, res) {
    if (!(utilitiesController.isStringNullUndefinedOrEmpty(req.params.siteId))) {
        const siteId = req.params.siteId;
        const query = tagSchema.find().where({siteId: siteId});
        utilitiesController.getObjectsFromDb(res, query);
    }
    else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};

exports.getSiteTagsByType = function (req, res) {
    if (!(utilitiesController.isStringNullUndefinedOrEmpty(req.params.siteId))
        || utilitiesController.isStringNullUndefinedOrEmpty(req.params.type)) {
        const siteId = req.params.siteId;
        const tagType = req.params.type;
        const query = tagSchema.find().where({
            $and: [{siteId: siteId}, {tagType: tagType}]
        });
        utilitiesController.getObjectsFromDb(res, query);
    }
    else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};

exports.getSiteTagsTypes = function (req, res) {
    if (!(utilitiesController.isStringNullUndefinedOrEmpty(req.params.siteId))) {
        const siteId = req.params.siteId;
        const query = tagSchema.find().where({siteId: siteId});
        const queryExePromise = query.exec();
        queryExePromise.then(doc => {
            let result = [];
            doc.forEach(tag => {
                result.push(tag.tagType);
            });
            result = Array.from(new Set(result));
            utilitiesController.returnResponse(res, HTTPStatus.OK, true, result);
        }).catch(err => {
            utilitiesController.returnErrorToClient(res, err);
        });
    }
    else {
        utilitiesController.returnResponse(res, HTTPStatus.BAD_REQUEST, false, responseMessage.illegalArgument);
    }
};

exports.getSiteBuildings = function (req, res) {
    const siteId = req.params.siteId;
    const query = spaceUnitSchema.find().where({
        $and: [{siteId: siteId}, {type: spaceUnitTypes.building}]
    });
    utilitiesController.getObjectsFromDb(res, query);
};

exports.getBuilding = function (req, res) {
    const buildingId = req.params.buildingId;
    const query = spaceUnitSchema.find().where({
        $and: [{buildingId: buildingId}, {type: spaceUnitTypes.building}]
    });
    utilitiesController.getObjectFromDb(res, query, buildingId, spaceUnitTypes.building)
};

exports.getBuildingFloors = function (req, res) {
    const buildingId = req.params.buildingId;
    const query = spaceUnitSchema.find().where({
        $and: [{buildingId: buildingId}, {type: spaceUnitTypes.floor}]
    });
    utilitiesController.getObjectsFromDb(res, query);
};

exports.getFloor = function (req, res) {
    const floorId = Number(req.params.floorId);
    let result = floorArrayCache[floorId];
    if (result === undefined) {
        const query = spaceUnitSchema.find().where(
            {floorId: floorId}
        );
        let queryExePromise = query.exec();
        queryExePromise.then(doc => {
                const dataToReturn = {};
                const floorUnit = doc.find(function (spaceUnit) {
                    return spaceUnit.type === spaceUnitTypes.floor;
                });
                if (floorUnit !== null && floorUnit !== undefined) {
                    dataToReturn.floorObject = floorUnit;
                    doc = doc.filter(function (spaceUnit) {
                        return spaceUnit.type !== spaceUnitTypes.floor;
                    });
                    dataToReturn.map = doc;
                    floorArrayCache[floorId] = dataToReturn;
                    utilitiesController.returnResponse(res, HTTPStatus.OK, true, dataToReturn);
                }
                else {
                    //return error to client no such floor on DB
                    throw {
                        message: responseMessage.spaceUnitNotFound,
                        httpStatus: HTTPStatus.NOT_FOUND
                    };
                }
            }
        ).catch(err => {
            utilitiesController.returnErrorToClient(res, err);
        });
    }
    else {
        utilitiesController.returnResponse(res, HTTPStatus.OK, true, result);
    }
};

exports.getFloorSensors = function (req, res) {
    const floorId = req.params.floorId;
    const query = sensorSchema.find().where(
        {floorUnitId: floorId}
    );
    let queryExePromise = query.exec();
    queryExePromise.then(doc => {
        utilitiesController.returnResponse(res, HTTPStatus.OK, true, doc);
    }).catch(err => {
        utilitiesController.returnErrorToClient(res, err);
    });
};

exports.getTagsOfSpaceUnit = function (req, res) {
    const spaceUnitId = req.params.spaceUnitId;
    const query = getTagsOfSpaceUnitPromise(spaceUnitId);
    let queryExePromise = query.exec();
    queryExePromise.then(doc => {
        utilitiesController.returnResponse(res, HTTPStatus.OK, true, doc);
    }).catch(err => {
        utilitiesController.returnErrorToClient(res, err);
    });
};

exports.getSpaceUnitInformation = function (req, res) { //get spaceUnitObject,tags and events of space unit
    const spaceUnitId = req.params.spaceUnitId;
    const dateTimeStart = req.params.dateTimeStart;
    const dateTimeEnd = req.params.dateTimeEnd;
    let promiseArray = [];
    promiseArray.push(getSpaceUnitByIdPromise(spaceUnitId));
    promiseArray.push(eventController.getEventsOfSpaceUnitPromise(spaceUnitId, dateTimeStart, dateTimeEnd));
    promiseArray.push(getTagsOfSpaceUnitPromise(spaceUnitId));
    Promise.all(promiseArray).then(doc => {
        let result = {};
        result.spaceUnit = doc[0];
        result.events = doc[1];
        result.tags = doc[2];
        utilitiesController.returnResponse(res, HTTPStatus.OK, true, result);
    }).catch(err => {
        utilitiesController.returnErrorToClient(res, err);
    });
};

exports.siteInformationForClient = function (req, res) {
    const siteId = Number(req.params.siteId);
    let result = siteInformationForClientCache[siteId];
    if (result === undefined) {
        result = {};
        let promiseArray = [];
        promiseArray.push(getSiteSpaceUnitPromise(siteId));
        promiseArray.push(getSiteSensors(siteId));
        promiseArray.push(getSiteTags(siteId));
        promiseArray.push(getSiteFloorsPromise(siteId));
        promiseArray.push(getSiteBuildingsPromise(siteId));
        promiseArray.push(getBuildingEntrance(siteId));
        Promise.all(promiseArray).then(doc => {
            result.siteRoomsForSearch = getSiteRoomsForSearch(doc[0]);
            result.siteSensors = doc[1];
            result.siteTags = doc[2];
            result.buildings = doc[3];
            result.floors = doc[4];
            result.buildingEntrance = doc[5];
            siteInformationForClientCache[siteId] = result;
            utilitiesController.returnResponse(res, HTTPStatus.OK, true, result);
        }).catch(err => {
            utilitiesController.returnErrorToClient(res, err);
        });
    }
    else {
        utilitiesController.returnResponse(res, HTTPStatus.OK, true, result);
    }
};

exports.clearDataCache = function () {
    floorArrayCache = [];
    siteInformationForClientCache = [];
};

function getAllSites() {
    return spaceUnitSchema.find().where({type: spaceUnitTypes.site});
}

function getAllSitesSensors() {
    return sensorSchema.find({});
}

function getSiteSensors(siteId) {
    return sensorSchema.find().where({siteUnitId: siteId});
}

function getSiteSpaceUnitPromise(siteId) {
    return spaceUnitSchema.find().where(
        {siteId: siteId}
    );
}

function getSiteRoomsForSearch(spaceUnits) {
    let result = [];
    spaceUnits.forEach(function (object) {
        if (ifRoomCanBySearch(object.type)) {
            result.push({
                type: object.type,
                spaceUnitId: object.spaceUnitId,
                buildingId: object.buildingId,
                floorId: object.floorId,
                name: object.name
            })
        }
    });
    return result;
}

function getSiteTags(siteId) {
    return tagSchema.find().where({siteId: siteId});
}

function getTagsOfSpaceUnitPromise(spaceUnitId) {
    return tagSchema.find({
        roomsIds: {$in: [spaceUnitId]}
    });
}

function getSpaceUnitByIdPromise(spaceUnitId) {
    return spaceUnitSchema.find({
        spaceUnitId: spaceUnitId
    })
}

function getSiteFloorsPromise(siteId) {
    return spaceUnitSchema.find().where({
        $and: [{siteId: siteId}, {type: spaceUnitTypes.building}]
    });
}

function getSiteBuildingsPromise(siteId) {
    return spaceUnitSchema.find().where({
        $and: [{siteId: siteId}, {type: spaceUnitTypes.floor}]
    });
}

function getBuildingEntrance(siteId) {
    return spaceUnitSchema.find().where({
        $and: [{siteId: siteId}, {type: spaceUnitTypes.buildingEntrance}]
    });
}

function ifRoomCanBySearch(roomType) {
    return !(roomType === spaceUnitTypes.corridor || roomType === spaceUnitTypes.building ||
    roomType === spaceUnitTypes.floor || roomType === spaceUnitTypes.site
    || roomType === spaceUnitTypes.elevator);
}