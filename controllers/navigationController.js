'use strict';
console.info("file navigationController.js loading");
const HTTPStatus = require('http-status');
const utilitiesController = require('./utilitiesController');
const spaceUnitSchema = require('./../db/spaceUnitSchema');
const responseMessage = require('../responseMessage.json');
const Graph = require('node-dijkstra');
const spaceUnitTypes = require('../spaceUnitTypes.json');

let pathArrayCache = {
    siteArray: [],
    siteArraySpecialMode: [],
    buildingArray: [],
    buildingArraySpecialMode: [],
    floorArray: [],
    floorArraySpecialMode: []
};


exports.getPath = function (req, res) {
    const spaceUnitIdFrom = req.params.from;
    const spaceUnitIdTo = req.params.to;
    const userAccessibility = Number(req.params.accessibility);
    let graph = findInPathArrayCache(spaceUnitIdFrom, spaceUnitIdTo, userAccessibility);
    if (graph !== null) {
        let result = graph.path(spaceUnitIdFrom, spaceUnitIdTo);
        utilitiesController.returnResponse(res, HTTPStatus.OK, true, result);
    }
    else {
        let query = null;
        let spaceUnitId = null;
        let searchLevel = calculateSearchLevel(spaceUnitIdFrom, spaceUnitIdTo);
        if (searchLevel === spaceUnitTypes.floor) {
            spaceUnitId = getFloorIdIdFromSpaceUnitId(spaceUnitIdTo);
            query = spaceUnitSchema.find().where({
                parentId: spaceUnitId
            })
        }
        else if (searchLevel === spaceUnitTypes.building) {
            spaceUnitId = getBuildingIdFromSpaceUnitId(spaceUnitIdTo);
            query = spaceUnitSchema.find().where({
                buildingId: spaceUnitId
            });
        }
        else if (searchLevel === spaceUnitTypes.site) {
            spaceUnitId = getSiteIdFromSpaceUnitId(spaceUnitIdTo);
            query = spaceUnitSchema.find().where({
                siteId: spaceUnitId
            });
        }
        if (query !== null) {
            let queryExePromise = query.exec();
            queryExePromise.then(doc => {
                graph = createGraph(doc, userAccessibility);
                addGraphToCache(graph, searchLevel, spaceUnitId, userAccessibility);
                let result = graph.path(spaceUnitIdFrom, spaceUnitIdTo);
                utilitiesController.returnResponse(res, HTTPStatus.OK, true, result);
            });
        }
        else {
            utilitiesController.returnErrorToClient(res, responseMessage.illegalArgument);
        }
    }
};

exports.clearDataCache = function () {
    pathArrayCache = {
        siteArray: [],
        siteArraySpecialMode: [],
        buildingArray: [],
        buildingArraySpecialMode: [],
        floorArray: [],
        floorArraySpecialMode: []
    };
};

function addGraphToCache(graph, searchLevel, spaceUnitId, userAccessibility) {
    if (searchLevel === spaceUnitTypes.floor) {
        if (userAccessibility === 0) {
            pathArrayCache.floorArray[spaceUnitId] = graph;
        }
        else {
            pathArrayCache.floorArraySpecialMode[spaceUnitId] = graph;
        }
    }
    else if (searchLevel === spaceUnitTypes.building) {
        if (userAccessibility === 0) {
            pathArrayCache.buildingArray[spaceUnitId] = graph;
        }
        else {
            pathArrayCache.buildingArraySpecialMode[spaceUnitId] = graph;
        }
    }
    else if (searchLevel === spaceUnitTypes.site) {
        if (userAccessibility === 0) {
            pathArrayCache.floorArray[spaceUnitId] = graph;
        }
        else {
            pathArrayCache.floorArraySpecialMode[spaceUnitId] = graph;
        }
    }
}

function createGraph(dataMap, userAccessibility) {
    const graph = new Graph();
    dataMap.forEach(function (unit) {
        if (unit.entrance !== null && unit.entrance !== undefined) {
            const cost = 1;
            let neighbor = null;
            const temp = {};
            for (let j = 0; j < unit.entrance.length; j++) {
                if (userAccessibility >= unit.entrance[j].approachable) {
                    neighbor = (unit.entrance[j].neighborId);
                    temp[neighbor] = cost;
                }
            }
            graph.addNode(unit.spaceUnitId, temp);
        }
    });
    return graph;
}

function calculateSearchLevel(spaceUnitIdFrom, spaceUnitIdTo) {
    let searchLevel = null;
    if (getFloorIdIdFromSpaceUnitId(spaceUnitIdFrom) === getFloorIdIdFromSpaceUnitId(spaceUnitIdTo)) {
        searchLevel = spaceUnitTypes.floor;
    }
    else if (getBuildingIdFromSpaceUnitId(spaceUnitIdFrom) === getBuildingIdFromSpaceUnitId(spaceUnitIdTo)) {
        searchLevel = spaceUnitTypes.building;
    }
    else if (getSiteIdFromSpaceUnitId(spaceUnitIdFrom) === getSiteIdFromSpaceUnitId(spaceUnitIdTo)) {
        searchLevel = spaceUnitTypes.site;
    }
    return searchLevel;
}

function findInPathArrayCache(spaceUnitIdFrom, spaceUnitIdTo, userAccessibility) {
    let searchLevel = calculateSearchLevel(spaceUnitIdFrom, spaceUnitIdTo);
    let graph = null;
    let spaceUnitId = null;
    if (searchLevel === spaceUnitTypes.floor) {
        spaceUnitId = getFloorIdIdFromSpaceUnitId(spaceUnitIdTo);
        if (userAccessibility === 0) {
            graph = pathArrayCache.floorArray[spaceUnitId];
        }
        else {
            graph = pathArrayCache.floorArraySpecialMode[spaceUnitId];
        }
    }
    else if (searchLevel === spaceUnitTypes.building) {
        spaceUnitId = getBuildingIdFromSpaceUnitId(spaceUnitIdTo);
        if (userAccessibility === 0) {
            graph = pathArrayCache.buildingArray[spaceUnitId];
        }
        else {
            graph = pathArrayCache.buildingArraySpecialMode[spaceUnitId];
        }
    }
    else if (searchLevel === spaceUnitTypes.site) {
        spaceUnitId = getSiteIdFromSpaceUnitId(spaceUnitIdTo);
        if (userAccessibility === 0) {
            graph = pathArrayCache.floorArray[spaceUnitId];
        }
        else {
            graph = pathArrayCache.floorArraySpecialMode[spaceUnitId];
        }
    }
    if (graph === undefined) {
        graph = null;
    }
    return graph;
}

function getSiteIdFromSpaceUnitId(spaceUnitId) {
    return Number(spaceUnitId.split('.')[0]);
}

function getBuildingIdFromSpaceUnitId(spaceUnitId) {
    return Number(spaceUnitId.split('.')[1]);
}

function getFloorIdIdFromSpaceUnitId(spaceUnitId) {
    return Number(spaceUnitId.split('.')[2]);
}