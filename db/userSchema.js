'use strict';
console.info("file userSchema.js loading");
const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const Schema = mongoose.Schema;

const User = new Schema({
    email: {type: String, unique: true, required: true},
    name: {type: String, unique: true, required: true},
    username: {type: String, unique: true, required: true},
    role:{type: String, required: true},
    password:String,
    updated: {type: Date, default: Date.now}
}, {collection: 'user'});

autoIncrement.initialize(mongoose.connection);
User.plugin(autoIncrement.plugin, 'user');
const userSchema = mongoose.model('user', User);
module.exports = userSchema;

