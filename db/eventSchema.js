'use strict';
console.info("file eventSchema.js loading");
const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const Schema = mongoose.Schema;

const event = new Schema({
    name: String,
    siteId: Number,
    description: String,
    spaceUnitIds: [String],
    usersIds: [Number],
    dateTimeStart: Date,
    dateTimeEnd: Date,
    updated: {type: Date, default: Date.now}
}, {collection: 'event'});

autoIncrement.initialize(mongoose.connection);
event.plugin(autoIncrement.plugin, 'event');
const eventSchema = mongoose.model('event', event);
module.exports = eventSchema;