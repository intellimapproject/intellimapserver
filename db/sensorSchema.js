'use strict';
console.info("file sensorSchema.js loading");
const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const Schema = mongoose.Schema;

const sensor = new Schema({
    identifier: String,
    uuid: String,
    minor: Number,
    major: Number,
    spaceUnitId: String,
    siteUnitId: Number,
    floorUnitId: Number,
    coordX: Number,
    coordY: Number,
    updated: {type: Date, default: Date.now}
}, {collection: 'sensor'});

autoIncrement.initialize(mongoose.connection);
sensor.plugin(autoIncrement.plugin, 'sensor');
const sensorSchema = mongoose.model('sensor', sensor);
module.exports = sensorSchema;