'use strict';
console.info("file tagSchema.js loading");
const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const Schema = mongoose.Schema;

const tag = new Schema({
    name: String,
    siteId:Number,
    roomsIds: [String],
    tagType:String,
    updated: {type: Date, default: Date.now}
}, {collection: 'tag'});

autoIncrement.initialize(mongoose.connection);
tag.plugin(autoIncrement.plugin, 'tag');
const tagSchema = mongoose.model('tag', tag);
module.exports = tagSchema;