'use strict';
console.info("file database.js loading");
//------------Connect tomongodbonmLabvia Mongoose--------------
let flagIsConnection = false;
const mongoose = require('mongoose');
const config = require('./../config.json');
const autoIncrement = require('mongoose-auto-increment');
const mongoUrl = 'mongodb://' + config.username + ':' + config.password + config.suffixMongoUrl;
//The server optionauto_reconnectis defaulted to true
const options = {
    server: {
        auto_reconnect: true
    }
};
mongoose.Promise = global.Promise;
mongoose.connect(mongoUrl, options);
const db = mongoose.connection;// a global connection variable
// Event handlers for Mongoose
db.on('error', function (err) {
    console.error('Mongoose: Error: ' + err);
});
db.on('open', function () {
    flagIsConnection = true;
    console.log("Connecting to database:" + config.suffixMongoUrl);
    console.info('Mongoose: Connection established');
});
db.on('disconnected', function () {
    flagIsConnection = false;
    console.info('Mongoose: Connection stopped,recconect');
    mongoose.connect(config.mongoUrl, options);
});
db.on('reconnected', function () {
    flagIsConnection = true;
    console.info('Mongoose reconnected!');
});

exports.isConnectedToDB = function () {
    return flagIsConnection
};