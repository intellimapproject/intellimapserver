'use strict';
console.info("file spaceUnitSchema.js loading");
const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const Schema = mongoose.Schema;

const spaceUnit = new Schema({
    spaceUnitId: String,
    siteId: Number,
    buildingId: Number,
    floorId: Number,
    parentId: Number,
    accessibility: Number,
    zones: [{
        widthUnit: Number,
        heightUnit: Number,
        topLeftX: Number,
        topLeftY: Number,
    }],
    entrance: [{
        xExitPosition: Number,
        yExitPosition: Number,
        direction: String,
        neighborId: String,
        approachable: Number
    }],
    type: String,
    name: String,
    capacity: Number,
    description: String,
    logoUrl: String,
    updated: {type: Date, default: Date.now}
}, {collection: 'spaceUnit'});

autoIncrement.initialize(mongoose.connection);
spaceUnit.plugin(autoIncrement.plugin, 'spaceUnit');
const spaceUnitSchema = mongoose.model('spaceUnit', spaceUnit);
module.exports = spaceUnitSchema;