'use strict';
console.info("file server.js loading");
const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const cookieParser = require('cookie-parser');
const app = express();
const port = process.env.PORT || 3000;
const mapConsumerController = require('./controllers/mapConsumerController');
const mapProducerController = require('./controllers/mapProducerController');
const searchController = require('./controllers/searchController');
const navigationController = require('./controllers/navigationController');
const userController = require('./controllers/userController');
const eventController = require('./controllers/eventController');
const rawBodySaver = function (req, res, buf, encoding) {
    if (buf && buf.length) {
        req.rawBody = buf.toString(encoding || 'utf8');
    }
};
app.set('port', port);
app.use(bodyParser.json({verify: rawBodySaver}));
app.use(bodyParser.urlencoded({verify: rawBodySaver, extended: true}));
app.use(bodyParser.raw({verify: rawBodySaver, type: '*/*'}));
app.use(cookieParser());
app.use(session({
    secret: 'secret',
    saveUninitialized: true,
    resave: true
}));
app.use(passport.initialize());
app.use(passport.session());

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.locals.user = req.user || null;
    console.log("Request url:" + req.url.toString());
    if (res.locals.user === null) {
        console.log("User not login in");
    }
    else {
        console.log("User:" + res.locals.user.username);
    }
    if (req.url.toString().search("mapProducer") !== -1) { // clearing cache if it mapProducer call
        mapConsumerController.clearDataCache();
        navigationController.clearDataCache();
        console.info("Cache data cleared");
    }
    next();
});

passport.use(new LocalStrategy(
    function (username, password, done) {
        userController.getUserByUsername(username, function (err, user) {
            if (err) throw err;
            if (!user) {
                return done(null, false, {message: 'Unknown User'});
            }
            userController.comparePassword(password, user.password, function (err, isMatch) {
                if (err) throw err;
                if (isMatch) {
                    return done(null, user);
                } else {
                    return done(null, false, {message: 'Invalid password'});
                }
            });
        });
    }));

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    userController.getUserById(id, function (err, user) {
        done(err, user);
    });
});

app.get('/api/v1/mapConsumer/site', mapConsumerController.getAllSites); // return all sites objects
app.get('/api/v1/mapConsumer/getAllSitesData', mapConsumerController.getAllSitesData);// return all sites objects and all sensors objects
app.get('/api/v1/mapConsumer/site/:siteId', mapConsumerController.getSite);
app.get('/api/v1/mapConsumer/site/:siteId/siteInformationForClient', mapConsumerController.siteInformationForClient);
app.get('/api/v1/mapConsumer/site/:siteId/tags', mapConsumerController.getSiteTags);
app.get('/api/v1/mapConsumer/site/:siteId/tags/:type', mapConsumerController.getSiteTagsByType);
app.get('/api/v1/mapConsumer/site/:siteId/tagsTypes', mapConsumerController.getSiteTagsTypes);
app.get('/api/v1/mapConsumer/site/:siteId/buildings', mapConsumerController.getSiteBuildings); // return array that represent building in site (only buildings objects)
app.get('/api/v1/mapConsumer/building/:buildingId', mapConsumerController.getBuilding); // return object that represent building
app.get('/api/v1/mapConsumer/building/:buildingId/floors', mapConsumerController.getBuildingFloors); // return array that represent building floors(only floors objects)
app.get('/api/v1/mapConsumer/floor/:floorId', mapConsumerController.getFloor); // return array that represent floor r to draw map
app.get('/api/v1/mapConsumer/floor/:floorId/sensors', mapConsumerController.getFloorSensors);
app.get('/api/v1/mapConsumer/getPath/:from/:to/:accessibility', navigationController.getPath);
app.get('/api/v1/mapConsumer/sensors/getAllSitesSensors', mapConsumerController.getAllSitesSensors);

app.get('/api/v1/mapConsumer/spaceUnit/:spaceUnitId/events/:dateTimeStart/:dateTimeEnd', eventController.getEventsOfSpaceUnit);
app.get('/api/v1/mapConsumer/spaceUnit/:spaceUnitId/tags', mapConsumerController.getTagsOfSpaceUnit);
app.get('/api/v1/mapConsumer/spaceUnit/:spaceUnitId/getSpaceUnitInformation/:dateTimeStart/:dateTimeEnd', mapConsumerController.getSpaceUnitInformation);
app.post('/api/v1/mapConsumer/search', searchController.search);
app.get('/api/v1/mapConsumer/getSpaceUnitsByIds', mapConsumerController.getSpaceUnitsByIds);
app.post('/api/v1/mapProducer/site', mapProducerController.createSite);
app.patch('/api/v1/mapProducer/site/:siteId', mapProducerController.editSite);
app.delete('/api/v1/mapProducer/site/:siteId', mapProducerController.deleteSite);
app.patch('/api/v1/mapConsumer/site/:siteId/logo/:uploadCode/:fileName', mapProducerController.setSiteLogo);
app.post('/api/v1/mapProducer/building', mapProducerController.createBuilding);
app.patch('/api/v1/mapProducer/building/:buildingId', mapProducerController.editBuilding);
app.delete('/api/v1/mapProducer/building/:buildingId', mapProducerController.deleteBuilding);

app.get('/api/v1/mapProducer/building/:buildingId/elevators', mapProducerController.getBuildingElevators);
app.get('/api/v1/mapProducer/building/:buildingId/stairs', mapProducerController.getBuildingStairs);

app.post('/api/v1/mapProducer/floor', mapProducerController.createFloor);
app.put('/api/v1/mapProducer/floor', mapProducerController.setFloorObjects);
app.patch('/api/v1/mapProducer/floor/:floorId', mapProducerController.editFloor);
app.delete('/api/v1/mapProducer/floor/:floorId', mapProducerController.deleteFloor);

app.post('/api/v1/mapProducer/sensor', mapProducerController.createSensor);
app.patch('/api/v1/mapProducer/sensor/:sensorId', mapProducerController.editSensor);
app.delete('/api/v1/mapProducer/sensor/:sensorId', mapProducerController.deleteSensor);

app.post('/api/v1/mapProducer/tag', mapProducerController.createTag);
app.patch('/api/v1/mapProducer/tag/:tagId', mapProducerController.editTag);
app.delete('/api/v1/mapProducer/tag/:tagId', mapProducerController.deleteTag);

app.post('/api/v1/mapProducer/tag/:tagId/spaceUnits', mapProducerController.addSpaceUnitsToTag);
app.delete('/api/v1/mapProducer/tag/:tagId/spaceUnits', mapProducerController.removeSpaceUnitsFromTag);

app.post('/api/v1/event/:eventId/spaceUnits', eventController.addSpaceUnitsToEvent);
app.delete('/api/v1/event/:eventId/spaceUnits', eventController.removeSpaceUnitsFromEvent);
app.post('/api/v1/event', eventController.createEvent);
app.get('/api/v1/event/getEventsForLoggedInUser/:siteId/:startTime/:endTime', eventController.getEventsForLoggedInUser);
app.get('/api/v1/event/getEventRoomsByEventName/:name/:startTime/:endTime', eventController.getEventRoomsByEventName);
app.post('/api/v1/user/register', userController.register);
app.post('/api/v1/user/login',
    passport.authenticate('local'),
    function (req, res) {
        userController.successLogin(req, res);
    });
app.post('/api/v1/user/logout', userController.logout);
app.get('/api/v1/user/getAllUsersList', userController.getAllUsersList);
app.get('/api/v1/user/getUsersByIds', userController.getUsersByIds);

/*
    for stress test
 */
app.get('/loaderio-8924bcf9d83f7c7f9ad5616b975e1f38', function (req, res) {
    res.status(200).send("loaderio-8924bcf9d83f7c7f9ad5616b975e1f38");
});
app.get('/loaderio-b4c9fe4a8fa3a011c46afd8b75012319', function (req, res) {
    res.status(200).send("loaderio-b4c9fe4a8fa3a011c46afd8b75012319");
});
app.use('/documentation', express.static('./documentation'));
app.listen(port, function () {
    console.log("Express server listening on port %d in %s mode", this.address().port, app.settings.env);
});